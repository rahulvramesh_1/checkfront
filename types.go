package checkfront

//GetBookingAPI - get booking path
const GetBookingAPI = "/api/3.0/booking/"

//Config - array used to store multiple accounts
type Config struct {
	CheckFrontAccounts []checkFrontAccount `json:"config"`
}

type checkFrontAccount struct {
	AuthKey    string `json:"auth_key"`
	Identifier string `json:"identifier"`
	EndPoint   string `json:"url"`
}

//GetBookingResponse - get booking by booking id json
type GetBookingResponse struct {
	Version   string `json:"version"`
	AccountID int    `json:"account_id"`
	HostID    string `json:"host_id"`
	Name      string `json:"name"`
	Locale    struct {
		ID       string `json:"id"`
		Lang     string `json:"lang"`
		Currency string `json:"currency"`
	} `json:"locale"`
	Request struct {
		Status    string  `json:"status"`
		Resource  string  `json:"resource"`
		ID        string  `json:"id"`
		Records   int     `json:"records"`
		Limit     int     `json:"limit"`
		Page      int     `json:"page"`
		Pages     int     `json:"pages"`
		Time      float64 `json:"time"`
		Timestamp float64 `json:"timestamp"`
		Method    string  `json:"method"`
	} `json:"request"`
	Booking struct {
		ID                string      `json:"id"`
		BookingID         int         `json:"booking_id"`
		AccountID         int         `json:"account_id"`
		StatusID          string      `json:"status_id"`
		CreatedDate       string      `json:"created_date"`
		Checkin           int         `json:"checkin"`
		Checkout          int         `json:"checkout"`
		CustomerName      string      `json:"customer_name"`
		CustomerAddress   interface{} `json:"customer_address"`
		CustomerCity      interface{} `json:"customer_city"`
		CustomerRegion    interface{} `json:"customer_region"`
		CustomerPostalZip interface{} `json:"customer_postal_zip"`
		CustomerCountry   interface{} `json:"customer_country"`
		CustomerEmail     string      `json:"customer_email"`
		CustomerPhone     string      `json:"customer_phone"`
		CustomerID        string      `json:"customer_id"`
		Token             string      `json:"token"`
		CurrencyID        string      `json:"currency_id"`
		PartnerID         string      `json:"partner_id"`
		Meta              struct {
			CustomerPhone     string `json:"customer_phone"`
			CustomerEmail     string `json:"customer_email"`
			CustomerName      string `json:"customer_name"`
			CustomerFirstName string `json:"customer_first_name"`
			CustomerLastName  string `json:"customer_last_name"`
			BookingLanguage   string `json:"booking_language"`
		} `json:"meta"`
		Language    string        `json:"language"`
		Bookmarked  int           `json:"bookmarked"`
		Tid         string        `json:"tid"`
		Provider    string        `json:"provider"`
		StartDate   int           `json:"start_date"`
		EndDate     int           `json:"end_date"`
		Taxes       []interface{} `json:"taxes"`
		DepositDue  string        `json:"deposit_due"`
		AmountPaid  string        `json:"amount_paid"`
		AmountDue   string        `json:"amount_due"`
		TaxTotal    string        `json:"tax_total"`
		TaxIncTotal string        `json:"tax_inc_total"`
		SubTotal    string        `json:"sub_total"`
		Total       string        `json:"total"`
		Items       struct {
			Num1 struct {
				StatusID    string        `json:"status_id"`
				ID          int           `json:"id"`
				Sku         string        `json:"sku"`
				Unit        string        `json:"unit"`
				Name        string        `json:"name"`
				StartDate   int           `json:"start_date"`
				EndDate     int           `json:"end_date"`
				Qty         string        `json:"qty"`
				CategoryID  int           `json:"category_id"`
				Taxes       []interface{} `json:"taxes"`
				TaxTotal    string        `json:"tax_total"`
				TaxIncTotal string        `json:"tax_inc_total"`
				ItemTotal   string        `json:"item_total"`
				SubTotal    string        `json:"sub_total"`
				Total       string        `json:"total"`
				Summary     string        `json:"summary"`
				Event       []int         `json:"event"`
				Param       struct {
					Qty struct {
						Qty  int    `json:"qty"`
						Name string `json:"name"`
					} `json:"qty"`
					Rooms struct {
						Qty  int    `json:"qty"`
						Name string `json:"name"`
					} `json:"rooms"`
				} `json:"param"`
				Discount interface{} `json:"discount"`
			} `json:"1"`
		} `json:"items"`
		Transactions interface{} `json:"transactions"`
	} `json:"booking"`
}

//UpdatePaymentResponse - response from the api
type UpdatePaymentResponse struct {
	Version   string `json:"version"`
	AccountID int    `json:"account_id"`
	HostID    string `json:"host_id"`
	Name      string `json:"name"`
	Locale    struct {
		ID       string `json:"id"`
		Lang     string `json:"lang"`
		Currency string `json:"currency"`
	} `json:"locale"`
	Request struct {
		Status   string `json:"status"`
		Resource string `json:"resource"`
		Scope    string `json:"scope"`
		ID       string `json:"id"`
		Records  int    `json:"records"`
		Limit    int    `json:"limit"`
		Page     int    `json:"page"`
		Pages    int    `json:"pages"`
		Method   string `json:"method"`
		Data     struct {
			StatusID string `json:"status_id"`
		} `json:"data"`
	} `json:"request"`
}
