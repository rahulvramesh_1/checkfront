package checkfront

import (
	"io/ioutil"
	"net/http"

	jsoniter "github.com/json-iterator/go"
)

//APISet -
type APISet interface {
	GetBooking(bookingID string) (*http.Response, error)
}

//Checkfront official struct
type Checkfront struct {
	config *Config
}

//New - new instance
func New(c Config) *Checkfront {

	return &Checkfront{config: &c}
}

//GetBooking - get the booking details by booking id and account identifier
func (cf *Checkfront) GetBooking(bookingID, account string) (GetBookingResponse, error) {

	var reponse GetBookingResponse

	//write simple and direct as we dont have any inejcton

	acc := cf.getAccountByName(cf.config.CheckFrontAccounts, account)

	//form the get url
	url := acc.EndPoint + GetBookingAPI + bookingID

	responseBody, err := cf.get(url, acc)

	if err != nil {
		return reponse, err
	}

	//mashel that data to struct
	err = jsoniter.Unmarshal(responseBody, &reponse)

	if err != nil {
		return reponse, err
	}

	return reponse, nil
}

//SetBookingStatus -
func (cf *Checkfront) SetBookingStatus(bookingID string, account string, status string) (UpdatePaymentResponse, error) {

	var reponse UpdatePaymentResponse
	var url string

	//account
	acc := cf.getAccountByName(cf.config.CheckFrontAccounts, account)

	url = acc.EndPoint + GetBookingAPI + bookingID + "/update?&status_id=" + status

	// if status == "void" {
	// 	url = acc.EndPoint + GetBookingAPI + bookingID + "/update?&status_id=VOID"
	// } else if status == "paid" {
	// 	url = acc.EndPoint + GetBookingAPI + bookingID + "/update?&status_id=PAID"
	// }
	responseBody, err := cf.get(url, acc)

	if err != nil {
		return reponse, err
	}

	//mashel that data to struct
	err = jsoniter.Unmarshal(responseBody, &reponse)

	if err != nil {
		return reponse, err
	}

	return reponse, nil

}

//SetBookingPaid -  set the booking as paid
func (cf *Checkfront) SetBookingPaid(bookingID string, account string) (UpdatePaymentResponse, error) {

	var reponse UpdatePaymentResponse

	//account
	acc := cf.getAccountByName(cf.config.CheckFrontAccounts, account)

	url := acc.EndPoint + GetBookingAPI + bookingID + "/update?set_paid=1&status_id=PAID"

	responseBody, err := cf.get(url, acc)

	if err != nil {
		return reponse, err
	}

	//mashel that data to struct
	err = jsoniter.Unmarshal(responseBody, &reponse)

	if err != nil {
		return reponse, err
	}

	return reponse, nil

}

func (cf *Checkfront) getAccountByName(cfa []checkFrontAccount, account string) checkFrontAccount {

	for _, checkfrontConfig := range cfa {

		if checkfrontConfig.Identifier == account {
			return checkfrontConfig
		}
	}

	return checkFrontAccount{}
}

func (cf *Checkfront) get(url string, accountConfig checkFrontAccount) ([]byte, error) {

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", "Basic "+accountConfig.AuthKey)
	req.Header.Add("Cache-Control", "no-cache")
	req.Header.Add("Host", "test-sandboxvrind.checkfront.com")
	req.Header.Add("Connection", "keep-alive")

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return []byte{}, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return []byte{}, err
	}

	return body, nil
}
